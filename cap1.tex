% Faz com que o ínicio do capítulo sempre seja uma página ímpar
\cleardoublepage

% Inclui o cabeçalho definido no meta.tex
\pagestyle{fancy}

% Números das páginas em arábicos
\pagenumbering{arabic}

\chapter{Introdução}

\textit{Neste capítulo são apresentados o contexto, os objetivos, as contribuições científicas e a estrutura do presente trabalho.}

\section{Contexto}

% O comando \label{nome} define o marcador da parte especificada.
% Você pode citar esta seção usando o comando \ref{nome}.
% O "~" evita uma quebra de linha entre as palavras.
A Engenharia de Software é um conjunto de métodos, 
técnicas e ferramentas necessárias à produção de software de 
qualidade para todas as etapas do ciclo de vida do produto \citep{krakowiak1985principes}.
Dentre essas etapas do ciclo de vida do software destaca-se a etapa de testes de software que, 
historicamente, tem sido o último subprocesso do clássico modelo em cascata
de desenvolvimento de software, onde, depois da implementação do software, 
normalmente uma equipe de testes escreve casos de teste (como testes unitários) 
a fim de validar o código implementado e com o intuito de encontrar erros \citep{myers1979art}. 
No entanto, utilizando a técnica de desenvolvimento de software Desenvolvimento
Guiado por Testes (do inglês: \textit{Test Driven Development} - TDD) 
é sugerido que o desenvolvedor escreva o teste antes mesmo da implementação \citep{aniche2012test}.
Além disso, TDD emprega os chamados testes de aceitação como ponto de partida 
para o processo de desenvolvimento. Esses testes de aceitação representam todos 
os cenários que o sistema final deve realizar. Embora, inicialmente, os casos de 
testes falham, pois não há a implementação da funcionalidade em questão, o projeto 
de software é considerado completo (aceito) se todos os testes de aceitação passam \citep{tdd-caelum, tdd-devmedia}.

Tem-se de notar que os testes de aceitação são diferentes dos testes unitários e 
não são uma alternativa. Enquanto os testes de unidade (teste de caixa-branca) validam 
se o sistema foi escrito da forma correta, os testes de aceitação (teste de caixa-preta) 
verificam se o software funciona de acordo com as necessidades do cliente, sem considerar 
como o sistema é implementado. Assim, o teste unitário valida se o software faz a coisa direito, 
ao passo que o teste de aceitação verifica se o software faz a coisa certa \citep{delamaro2007introduccao}. 
Consequentemente, os testes de unidade são geralmente escritos pelos desenvolvedores e as partes interessadas 
não vão tomar conhecimento deles, enquanto os testes de aceitação são escritos pelas partes 
interessadas e são discutidos com os desenvolvedores como parte da especificação.

Recentemente, a técnica de desenvolvimento de software Desenvolvimento Guiado por Comportamento
(do inglês: \textit{Behavior Driven Development} - BDD)
foi proposta como uma resposta aos problemas que surgiram com TDD ao aplicar práticas 
ágeis em diferentes projetos de software. Os principais obstáculos para os programadores eram 
saber por onde começar, o que testar e o que não testar, quanto testar de cada vez, como dar 
nomes aos testes e como entender a causa de uma falha \citep{bdd}. Inicialmente concebido por Dan 
North\footnote{\url{http://dannorth.net/}}, no final de 2003, BDD usa a linguagem natural como meio de comunicação 
ubíqua (do inglês: \textit{ubiquitous language})\footnote{Linguagem clara, natural e compartilhada entre os membros da 
equipe de desenvolvimento e os clientes.} para descrever os testes de aceitação, 
por meio de cenários. Assim, o uso da linguagem natural, garante um entendimento comum 
sobre o sistema a ser desenvolvido entre todos os membros do projeto – particularmente entre 
desenvolvedores, setores de qualidade e pessoas não-técnicas ou de negócios \citep{spbdd}.

%EVANS, E. Domain­Driven Design: Tackling Complexity in the Heart of Software. New York: Addison­Wesley, 2004.

%Com base nos cenários que são descritos pelos testes de aceitação, a equipe de desenvolvimento mapeiam 
%as sentenças escritas em linguagem natural para implementação de casos de teste e esqueletos 
%de códigos. Normalmente esses primeiros passos são realizados de forma manual e, portanto, é um processo 
%demorado e sujeito a erros. No entanto, toda a informação que é necessária para executar 
%essas etapas é, em princípio, já incluída na descrição natural.

Com base nos cenários que são descritos pelos testes de aceitação, os analistas mapeiam
as sentenças escritas em linguagem natural para a modelagem do software e os programadores implementam
os casos de testes e esqueletos de códigos. Normalmente esses primeiros passos são realizados de forma manual e, portanto, é um processo 
demorado e propício a erros. No entanto, toda a informação que é necessária para executar 
essas etapas é, em princípio, já incluída na descrição natural.

\section{Objetivos}

O principal objetivo deste trabalho consiste em criar uma ferramenta que auxilie a equipe de desenvolvimento de software
no processo de extrair automaticamente a estrutura do software a partir de estórias de usuário 
escritas em Português Brasileiro (PB) e conforme o padrão BDD\footnote{\url{http://dannorth.net/whats-in-a-story/}}, 
utilizando técnicas de Processamento de Linguagem Natural (PLN). Propõe-se um fluxo de projeto onde o usuário entra com uma estória de usuário 
e a ferramenta processa sentença por sentença e sugere a criação de blocos 
de código java\footnote{\url{http://docs.oracle.com/javase/tutorial/java/}}, tais como classes, atributos, métodos e relacionamentos, 
além dos diagramas de classe e de sequência. O usuário pode aceitar essas sugestões ou 
editar a estória de usuário para recriar esses artefatos, o que leva a ter um maior controle sobre a 
criação da estrutura do software.

Utilizando esse fluxo de projeto, alcança-se as seguintes vantagens:
\begin{itemize}
  \item Tendo apenas cenários descritos em linguagem natural, os primeiros passos para a implementação da 
estrutura geral do sistema pode ser complicado. Entretanto, analisar passo a passo os cenários com a assistência de uma ferramenta
deverá auxiliar o início do processo de desenvolvimento.
  \item Permite se concentrar nas razões pelas quais o código deve ser criado, e 
  não em detalhes técnicos dando, assim, foco em qualidade e em entrega rápida 
  de software de valor \citep{spbdd}.
  \item Os riscos de ambiguidades podem ser minimizados quando a informação é analisada 
  por meio de técnicas de processamento de linguagem natural, pois é possível que a 
  informação mal interpretada pela ferramenta possa ser a mesma informação mal interpretada 
  pela equipe de desenvolvimento ou parte interessada.
  \item A nossa abordagem fornece ao usuário um \textit{feedback} após a análise da estória de usuário. 
  Como resultado, o usuário pode refazer as decisões da ferramenta, se necessário.
\end{itemize}

\section{Contribuições}

Este trabalho foi avaliado pela comunidade acadêmica através da publicação do seguinte artigo:

\begin{itemize}
  \item \textbf{\textit{Desenvolvimento Guiado por Comportamento usando Processamento de Linguagem Natural.}} (ERIPI, 2016).
\end{itemize}

Além disso, destaca-se a ferramenta ClasSeq que contribui para geração dos diagramas de classe, de sequência e do código java, a partir de estórias de usuário escritas em 
PB e sob o padrão BDD. Essa ferramenta encontra-se disponível em: \url{https://goo.gl/nYSNRA}.

\section{Estrutura do Trabalho}

O restante do trabalho está organizado da seguinte forma: 

No Capítulo \ref{cap2} são apresentos o referencial teórico e os trabalhos relacionados. No Capítulo \ref{cap3} são detalhados os padrões linguísticos. 
O Capítulo \ref{cap4} descreve a ferramenta ClasSeq. No Capítulo \ref{cap5} é discutido uma avaliação da ferramenta ClasSeq. Finalmente, 
no Capítulo \ref{cap6} são apresentadas as conclusões, as contribuições da pesquisa, os trabalhos futuros e as limitações e dificuldades encontradas.