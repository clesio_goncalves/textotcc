\pagestyle{empty}
\cleardoublepage
\pagestyle{fancy}

\chapter{A Ferramenta ClasSeq}\label{cap4}

\textit{Este capítulo tem como objetivo apresentar a ferramenta ClasSeq desenvolvida para a geração automática dos diagramas de classe, de sequência
e do código java a partir de estórias de usuário.}

\section{Introdução}\label{cap4:introducao}

A ferramenta ClasSeq implementa os padrões linguísticos, descritos no Capítulo \ref{cap3}, em conjunto com técnicas de
Processamento de Linguagem Natural (PLN), apresentadas na Seção \ref{cap2:pln}, com o objetivo de gerar os diagramas de classe, de sequência
e o código java automaticamente.

A ferramenta foi desenvolvida utilizando-se a linguagem de programação Java em conjunto com a tecnologia
Java EE (\textit{Java Enterprise Edition})\footnote{\url{http://www.oracle.com/technetwork/java/javaee/overview/index.html}} e o
framework Spring MVC\footnote{\url{http://springsource.org}}. Essa ferramenta está disponível no link
\url{https://goo.gl/nYSNRA}. Ela é acessível por qualquer dispositivo, desde um celular até um Desktop,
pois suporta diferentes resoluções de tela, com base no conceito de \textit{Web Design Responsivo} proposto por \cite{marcotte2011responsive}.

De modo geral, a ferramenta ClasSeq recebe como entrada uma estória de usuário no padrão BDD, analisa os padrões linguísticos e
gera, de forma automática, os diagramas de classe, de sequência e o código java. Na Figura \ref{cap4:figura1} é apresentada uma visão geral da abordagem proposta.

\begin{figure}[h]
    \centering
    \includegraphics[width=15cm]{imagens/cap4/fluxograma.png}
    \caption{Visão geral da abordagem proposta}
    \label{cap4:figura1}
\end{figure}

Como mostra a Figura \ref{cap4:figura1}, o usuário informa uma estória de usuário e, em seguida, submete-a para processamento dos
padrões linguísticos para posterior geração dos artefatos propostos.

Com o objetivo de auxiliar o usuário na identificação e correção de possíveis erros no diagrama de classes,
exibiu-se uma representação gráfica desse diagrama que contém a mesma estrutura do diagrama exportado para as ferramentas
ArgoUML\footnote{\url{http://argouml.tigris.org/}} o Modelio\footnote{\url{https://www.modelio.org/}}.
Essa imagem foi criada com o auxílio da ferramenta online yUML\footnote{\url{http://yuml.me/}}.
Dessa forma, caso necessário, o usuário poderá editar a estória
para a geração correta do diagrama ou modificá-lo em uma dessas ferramentas. Entretanto, não foi possível utilizar uma imagem que
exibisse o diagrama de sequência, pois não foi encontrada ferramenta disponível que gere uma imagem dele.

Vale ressaltar que a utilização de linguagem natural traz consigo o problema da ambiguidade \citep{rafael2014pln}.
Com isso, a ferramenta ClasSeq pode gerar estruturas diferentes da intenção do usuário, assim como um analista modela uma
ferramenta diferente da especificação de requisitos, por exemplo.

\section{Arquitetura da Ferramenta}\label{cap4:arquitetura}

Na Figura \ref{cap4:figura2} é mostrada a arquitetura da ferramenta ClasSeq para a geração dos diagramas de classe, de sequência e o código java.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{imagens/cap4/arquitetura.png}
    \caption{Arquitetura da ferramenta ClasSeq}
    \label{cap4:figura2}
\end{figure}

Como dito anteriormente, a ferramenta recebe como entrada uma estória de usuário escrita em português brasileiro, em seguida a ferramenta realiza um 
pré-processamento nesse texto, removendo as palavras reservadas ao padrão BDD, espaços em branco, mensagens e as locuções verbais compostas por ``deve'' + verbo.

Após o pré-processamento, inicia-se a etapa de etiquetagem do texto. Nessa etapa,  
as palavras do texto são separadas em \textit{tokens} e, em seguida, esses \textit{tokens} são etiquetados de
acordo com a classe gramatical, utilizando, em ambos os processos, o etiquetador TreeTagger.
%por ser gratuito e ter uma precisão superior a 91\% para o português brasileiro \citep{gamallo2013freeling}. 

Em seguida, os \textit{tokens} etiquetados são passados para a análise de padrões linguísticos pré-definidos para identificar os componentes dos diagramas de
classe e de sequência utilizando uma base de dados para ajudar nesse processo. 
Esses padrões facilitam a aproximação dos artefatos gerados de forma automática, com os criados manualmente por humanos \citep{silva2008paradigma, rafael2014pln}.
Eles foram implementados com o auxílio da ferramenta ANTLR e são detalhados no Capítulo \ref{cap3}.

Por fim, na etapa de modelagem e codificação, a ferramenta gera e exporta os diagramas de classe, de sequência e o código java.
O diagrama de classes é exportado tanto para o ArgoUML quanto para o Modelio, o digrama de sequência, 
no entanto, é exportado apenas para o Modelio e o código java pode ser visualizado e editado por qualquer 
editor de texto simples. Assim, caso necessário, o usuário poderá modificar os artefatos
gerados em ferramentas especializadas.

\section{Detalhes da Ferramenta ClasSeq}\label{cap4:detalhes}

Na Figura \ref{cap4:figura3} é apresentada a tela inicial da ferramenta. A partir dela o usuário
pode escrever suas estórias ou carregá-las de um arquivo de texto na ferramenta.

A ferramenta ClasSeq pode receber como entrada arquivos nos formatos TXT, STORY, DOC e PDF.
Para a leitura de Documentos do Word (DOC), utilizou-se a biblioteca
Apache POI\footnote{\url{http://poi.apache.org/}},
e para a leitura de arquivos PDF, utilizou-se a biblioteca de código aberto
Apache PDFBox™\footnote{\url{http://pdfbox.apache.org/}}. Além disso, utilizou-se a API
\textbf{java.io}\footnote{\url{https://docs.oracle.com/javase/8/docs/api/java/io/package-summary.html}}
para a leitura de Documentos de Texto (TXT) e
estórias de usuário do framework JBehave\footnote{\url{http://jbehave.org/}} (STORY).

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{imagens/cap4/home.png}
    \caption{Tela inicial da ferramenta}
    \label{cap4:figura3}
\end{figure}

A ação de clicar no botão `Enviar', na parte inferior da Figura \ref{cap4:figura3}, submete a estória de usuário para a
geração do produto final, ou seja, os diagramas de classe, de sequência e o código java. Nas subseções \ref{cap4:diagramas} e
\ref{cap4:codigo} são detalhados os processos de geração desses artefatos.

Na Figura \ref{cap4:figura4} é apresentada a tela final da ferramenta após a submissão da estória. Destacam-se nessa imagem as
opções de \textit{download} dos artefatos gerados e a representação gráfica do diagrama de classes, criada com o auxílio da ferramenta yUML.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{imagens/cap4/exibir.png}
    \caption{Tela final da ferramenta}
    \label{cap4:figura4}
\end{figure}

Na Figura \ref{cap4:figura4} é possível observar que há duas opções de \textit{download} de arquivos XMI (\textit{XML Metadata Interchange}), XMI v1.2 e XMI v2.1,
esses arquivos representam a estrutura dos diagramas UML exportados para as ferramentas ArgoUML e Modelio, respectivamente nessas versões.
Esses arquivos serão detalhados na subseção \ref{cap4:diagramas}.

\subsection{Geração dos Diagramas de Classe e de Sequência}\label{cap4:diagramas}

Para gerar os diagramas de classe e de sequência, é necessário escrever ou fazer \textit{upload} da estória de usuário e clicar no botão `Enviar'.
Após o clique, essa estória é analisada pelos padrões linguísticos detalhados no Capítulo \ref{cap3} e a ferramenta ClasSeq irá gerar esses
diagramas automaticamente.

Nesse processo, a ferramenta recebe os componentes identificados pelos padrões linguísticos em uma estrutura de dados do tipo
\textit{HashMap}\footnote{\url{https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html}}, onde a chave é a classe e o
valor é um conjunto de atributos e métodos. Os relacionamentos também armazenados dentro de um \textit{HashMap},
onde a chave é o tipo de relacionamento e o valor é um conjunto de pares de classes que compõem os relacionamentos.
Na sequência, essas estruturas são passadas como entrada para a geração e exportação dos diagramas
para o ArgoUML e o Modelio, ambas são ferramentas de modelagem de diagramas UML, de código aberto e extremamente utilizadas no meio acadêmico.

\begin{figure}[h]
    \centering
    \includegraphics[width=15cm]{imagens/cap4/estoria_usuario.png}
    \caption{Exemplo de estória de usuário retirado de \cite{campos}}
    \label{cap4:figura5}
\end{figure}

A Figura \ref{cap4:figura6} contém duas subfiguras que apresentam o mesmo diagrama de classes em ferramentas diferentes.
Na subfigura \ref{cap4:figura6a} é exibido o diagrama no ArgoUML e na subfigura \ref{cap4:figura6b} no Modelio.
Esses diagramas de classes foram gerados a partir da análise da estória de usuário apresentada na Figura \ref{cap4:figura5}.

\begin{figure}[h]
\centering
\subfloat[ArgoUML]{
\includegraphics[height=4.5cm]{imagens/cap4/argouml.png}
\label{cap4:figura6a}
}
\quad %espaco separador
\subfloat[Modelio]{
\includegraphics[height=4.5cm]{imagens/cap4/classe_modelio.png}
\label{cap4:figura6b}
}
\caption{Diagrama de classes: ArgoUML e Modelio}
\label{cap4:figura6}
\end{figure}

Na subfigura \ref{cap4:figura6b} também é exibida a visibilidade dos atributos e métodos modelada com auxílio da ferramenta Modelio.
Nessa subfigura, os atributos possuem visibilidade privada, representada pelo símbolo de menos (-) e significa que somente os objetos da própia classe do atributo poderão utilizá-lo.
Por outro lado, os métodos são públicos, pois são representados pelo sinal de mais (+) e, com isso, podem ser acionados por objetos de qualquer classe
\citep{booch2005uml}. Esse processo de modelagem foi adotado em toda geração do diagrama de classes.

Os diagramas de classe e sequência são exportados em aquivos XMI, que é um padrão da OMG
para troca de informações baseadas em XML\footnote{\url{http://www.w3.org/XML/}}.
Esses arquivos são exportados em duas versões: v1.2 e v2.1. Isso porque as versões desses arquivos são incompatíveis
e as ferramentas de modelagem UML utilizam diversas versões.

Cabe ressaltar que a versão v1.2 exportada é específica para o ArgoUML e contém apenas a estrutura do diagrama de classes,
pois o ArgoUML ainda não oferece suporte para a importação de arquivo XMI que define o comportamento do diagrama
de sequência. Entretanto, a versão v2.1, compatível com o Modelio, contém toda a estrutura de ambos os diagramas.

Como dito no Capítulo \ref{cap3}, os padrões linguísticos não são capazes de identificar associações unária e ternária, agregação, composição,
generalização, dependência e realização. Além disso, caso o tipo de dado do atributo não seja identificado, esse será do tipo \textit{String}.
Contudo, caso necessário, o usuário poderá editar essas propriedades conforme desejar através do ambiente de modelagem.

Na Figura \ref{cap4:figura7} é apresentado o diagrama de sequência gerado a partir da estória de usuário presente na Figura \ref{cap4:figura5}.
Conforme descrito no Capítulo \ref{cap3}, esse diagrama é construído com base no diagrama de classes e no papel da estória de usuário,
não utilizando, assim, nenhum padrão linguístico para sua geração. Com isso, para o processo de construção do diagrama de sequência,
a ferramenta ClasSeq recebe como entrada os \textit{HashMaps} com as informações do diagrama de classes e dos relacionamentos.

\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{imagens/cap4/sequencia_modelio.png}
    \caption{Diagrama de sequência: Modelio}
    \label{cap4:figura7}
\end{figure}

Como visto na Figura \ref{cap4:figura7}, as mensagens representam a comunicação entre um ator e um
objeto, onde o ator produz um evento que aciona um método em um objeto.

\subsection{Geração do código java}\label{cap4:codigo}

Assim como no diagrama de sequência, o processo de geração do código java também recebe a estrutura das classes e dos relacionamentos
armazenados em \textit{HashMaps}. O código java corresponde a implementação do modelo conceitual de classes na linguagem de
programação java, uma das linguagens mais populares e utilizadas do mundo \citep{ranking2016, ranking20162, ranking20163, ranking2015github, ranking2015ieee}.
Na Figura \ref{cap4:figura8} é exibido o código java gerado a partir da estória de usuário contida na
Figura \ref{cap4:figura5}.

\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{imagens/cap4/codigo_java.png}
    \caption{Código java gerado}
    \label{cap4:figura8}
\end{figure}

Como é mostrado na Figura \ref{cap4:figura8}, os atributos são privados (não podem ser acessados fora da classe) e
os métodos são públicos (permite que todos as classes acessem-os).
Na orientação a objetos, é prática quase que obrigatória proteger os atributos com o modificador de acesso \textit{private} \citep{deitel2010}.
Cada classe é responável por controlar os seus atributos, facilitando, assim, futuras mudanças no código, conforme necessidade do usuário.
Esse procedimento foi aplicado a todo código java gerado pela ferramenta.

\section{Considerações Finais}\label{cap4:consideracoes}

Este capítulo apresentou a ferramenta ClasSeq que implementa os padrões linguísticos para gerar, de forma automática, os
diagramas de classe e de sequência e o código java a partir de estórias de usuário. Esses artefatos gerados irão ajudar a equipe de desenvolvimento
nos primeiros passos do processo de desenvolvimento de software. Além disso, o usuário pode aceitar essas sugestões ou editar essa estória
para recriar o produto final ou, ainda, alterá-lo em ferramentas apropriadas, o que leva a ter um maior controle sobre a
criação da estrutura do software.

O código java gerado corresponde a mesma estrutura do modelo conceitual de classes, apresentado na forma de diagrama UML, visto que para gerá-lo
é necessário passar os elementos do diagrama de classes em uma estrutura de dados. No próximo capítulo será abordada uma avaliação da ferramenta ClasSeq.