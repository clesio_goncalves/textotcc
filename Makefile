FILE = tcc

all :
	pdflatex $(FILE).tex
	makeindex $(FILE).nlo -s nomencl.ist -o $(FILE).nls
	bibtex $(FILE).aux
	pdflatex $(FILE).tex
	pdflatex $(FILE).tex
	make clean

draft:
	pdflatex -interaction=batchmode $(FILE).tex
	make clean

clean:
	rm -rf *.aux *.bbl *.toc *.out *.log *.lof *.lot *.blg *.ilg *.backup

git commit -m 'Termino da conclusao'
git push textotcc master

makeindex tcc.nlo -s nomencl.ist -o tcc.nls
pdflatex tcc.tex
